<!DOCTYPE html>
<html lang="pt_br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Security Key</title>
    <link rel="shortcut icon" href="/pix/icon.ico" />
    <link href='/fonts/Mohave.otf' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/style/bootstrap.css">
    <link rel="stylesheet" href="/style/key.css">
    <link rel="stylesheet" href="/style/animate.css">

    <script src="/js/jquery.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script src="/js/key.js" type="javascript"></script>


</head>
<body>
<?php
include_once "header.html";
?>
<!--content-->
<div id="key-content" class="col-md-12">
    <div>
        <div id="bg-key" class="animated bounceInLeft">
            <img src="pix/bg.jpg" class="img-responsive" />
        </div>
        <?php
        include_once "./content/about.html";
        include_once "./content/products.html";
        include_once "./content/contact.html";

        ?>
    </div>
</div>


</body>
<?php
include_once "footer.html";
?>