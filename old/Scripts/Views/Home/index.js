﻿$(document).ready(function () {

    $("#NovoTelefone_DDD").mask("(00)");


    var mascaraTelefone = function (val) {
        return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
    },
    spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(mascaraTelefone.apply({}, arguments), options);
        }
    };

    $('#NovoTelefone_Numero').mask(mascaraTelefone, spOptions);
    //Excluir itens do grid de telefones do fiscalizado    
    $("#GridTelefones").on("click", "a.excluir-telefone", function (e) {
        e.preventDefault();
        if (confirm("Deseja realmente excluir o telefone?")) {

            $.ajax({
                type: "POST",
                url: $(this).data('url-action'),
                data: $('form').serialize(),
                success: function (response) {
                    $("#GridTelefones").html(response);
                }
            });
        }
    });

    //Configuração de evento para adicionar dinamicamente os telefones do fiscalizado
    $("#AdicionarTelefone").on("click", function (e) {
        e.preventDefault();

        $("form").each(function () { $(this).validate().settings.ignore = ":hidden, .input-email"; });
        if ($("input.input-telefone, select.input-telefone").valid()) {
            $.ajax({
                type: "POST",
                url: $(this).data('url-action'),
                data: $('form').serialize(),
                success: function (response) {
                    $("#GridTelefones").html(response);
                    $("#NovoTelefone_IdTipoTelefone").val("");
                    $("#NovoTelefone_DDD").val("");
                    $("#NovoTelefone_Numero").val("");
                }
            });
        }

        $("form").each(function () { $(this).validate().settings.ignore = ":hidden, .input-telefone, .input-email"; });
    });
});